from src.airplane import Airplane


def test_initial_state_a_plane():
    airplane1 = Airplane('Airbus', 300)
    assert airplane1.brand == 'Airbus'
    assert airplane1.total_available_seats ==300
    assert airplane1.total_mileage == 0
    assert airplane1.occupied_seats == 0


def test_flied_correct_distance_once():
    airplane1 = Airplane("Airbus", 300)
    airplane1.fly(5000)
    assert airplane1.total_mileage == 5000


def test_flied_correct_distance_twice():
    airplane1 = Airplane("Airbus", 300)
    airplane1.fly(5000)
    airplane1.fly(3000)
    assert airplane1.total_mileage == 8000


def test_is_service_required_after_9999():
    airplane1 = Airplane("Airbus", 300)
    airplane1.fly(9999)
    assert not airplane1.is_service_required()


def test_is_service_required_after_10001():
    airplane1 = Airplane("Airbus", 300)
    airplane1.fly(10001)
    assert airplane1.is_service_required()


def test_airplane_board_passengers():
    airplane2 = Airplane("Boeing", 200)
    airplane2.board_passengers(180)
    assert airplane2.get_available_seats() == 20
    assert airplane2.occupied_seats == 180


def test_airplane_board_passangers_is_overloaded():
    airplane2 = Airplane("Boeing", 200)
    airplane2.board_passengers(201)
    assert airplane2.get_available_seats() == 0
    assert airplane2.occupied_seats == 200
