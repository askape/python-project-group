class Airplane:
    def __init__(self, brand, total_able_seats):
        self.brand = brand
        self.total_available_seats = total_able_seats
        self.total_mileage = 0
        self.occupied_seats = 0

    def fly(self, distance):
        self.total_mileage += distance

    def is_service_required(self):
        return self.total_mileage > 10000


    def board_passengers(self, number_of_passangers):
        if number_of_passangers + self.occupied_seats <= self.total_available_seats:
            self.occupied_seats += number_of_passangers
        else:
            self.occupied_seats = self.total_available_seats

    def get_available_seats(self):
        return self.total_available_seats - self.occupied_seats

if __name__ == '__main__':
    airplane1 = Airplane('Airbus', 300)
    airplane1 = Airplane('Boeing', 200)

    airplane1.fly(10001)
    print(airplane1.total_mileage)
    print(airplane1.is_service_required())
    airplane1.board_passengers(200)
    print(airplane1.occupied_seats)
    print(airplane1.get_available_seats())
